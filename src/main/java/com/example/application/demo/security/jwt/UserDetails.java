package com.example.application.demo.security.jwt;

public class UserDetails {

    private Object authorities;

    public Object getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Object authorities) {
        this.authorities = authorities;
    }
}
