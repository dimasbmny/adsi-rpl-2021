package com.example.application.demo.security.jwt;


import com.example.application.demo.service.UserPrinciple;
import com.sun.org.apache.xml.internal.security.algorithms.SignatureAlgorithm;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.sql.RowSet;
import java.security.SignatureException;
import java.util.Date;

@Component
public class JwtProvider {
    private static final Logger logger = LoggerFactory.getLogger(JwtProvider.class);
    private static final Object HS512 = null;

    @Value("{.app.jwtSecret}")
    private String jwtSecret;

    @Value("{.app.jwtExpiration}")
    private String jwtExpiration;
    private Date expiration;

    public String generateJwtToken(Authentication authentication) {
        UserPrinciple userPrinciple = (UserPrinciple) authentication.getPrincipal();
        RowSet userPrincipal = null;
        return Jwts.parser()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime()+jwtExpiration))
                .signWith(HS512, jwtSecret)
                .compact();
    }

    private void setIssuedAt(Date date) {

    }

    private void compact() {
    }

    private void signWith(SignatureAlgorithm hs512, String jwtSecret) {
    }

    public String getUserNameFromJwtToken(String Token) {
        String token = null;
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody().getSubject();
    }

public boolean validateJwtToken(String authtoken) {
        try {
            assert JwtAuthTokenFilter.parser() != null;
            JwtAuthTokenFilter.parser().setSigningKey(jwtSecret).parseClaimsJws(authtoken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid Jwt signature -> Message: {}", e);
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token -> Message: {}", e);
        } catch (ExpiredJwtException e) {
            logger.error("Expired JWT token -> Message{}", e);
        } catch (UnsupportedJwtException e) {
            logger.error("Unsupported JWT token -> Message{}", e);
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty -> Massage: {}", e);
        }

        return false;


        }


    public Object validateJwtToken(Object jwt) {
        return null;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public Date getExpiration() {
        return expiration;
    }

    private class MalformedJwtException extends Throwable {
    }

    private class ExpiredJwtException extends Throwable {
    }

    private class UnsupportedJwtException extends Throwable {
    }

    private class HS512 {
    }

    private static class Jwts {
        public static Object parser() {
            return null;

        }
    }
}
