package com.example.application.demo.model;

public @interface Size {
    int min();

    int max();
}
