package com.example.application.demo.service;

import com.example.application.demo.model.Role;
import com.example.application.demo.model.User;
import com.example.application.demo.security.jwt.UserDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public abstract class UserPrinciple extends UserDetails {
    private static final long serialversionUID = 1L;
    public static String getUsername;

    private Long id;

    private String name;

    private String username;

    private String email;

    @JsonIgnore
    private String password;

    private Collection authorities;

    public UserPrinciple(Long id, String name, String username, String email, String password, Collection authorities) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.authorities = authorities;

    }

    public static UserPrinciple build(User user) {
        List<String> authorities = new ArrayList<>();
        for (Role role : user.getRoles()) {
            Object authority = new SimpleGrantedAuthority(role.getName().name()).getAuthority();
            authorities.add((String) authority);
        }

        final UserPrinciple userPrinciple = new UserPrinciple(
                user.getId(),
                user.getName(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                authorities
        ) {

            public Long getId() {
                return getId();
            }

            public String getNAme() {
                return getNAme();
            }

            public String getEmail() {
                return getEmail();
            }

            @Override
            public void setAuthorities(Object authorities) {
                super.setAuthorities(authorities);
            }

            public String getUsername() {
                return getUsername();
            }

            @Override
            protected Object clone() throws CloneNotSupportedException {
                return super.clone();
            }

            public String getPassword() {
                return getPassword();
            }

            @Override
            public Collection getAuthorities() {
                return authorities;
            }

            @Override
            public int hashCode() {
                return super.hashCode();
            }

            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public String toString() {
                return super.toString();
            }

            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            protected void finalize() throws Throwable {
                super.finalize();
            }

            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return true;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                UserPrinciple user = (UserPrinciple) o;
                return Objects.equals(getId(), user.id);
            }
        };
        return userPrinciple;
    }

    public abstract boolean isEnabled();
}