package com.example.application.demo.service;

import com.example.application.demo.repository.UserRepository;
import com.example.application.demo.security.jwt.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Transactional
    public UserDetails loadUserByUsername(String username)
        throws UsernameNotFoundException {

        com.example.application.demo.model.User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with -> username or email : " + username));

        return UserPrinciple.build(user);

    }

    private class UsernameNotFoundException extends Exception {
        public UsernameNotFoundException(String s) {

        }
    }

    private class User {
    }
}
