package com.example.application.demo.repository;

import com.example.application.demo.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    default Optional<Role> findByName() {
        return findByName();

    }

}

